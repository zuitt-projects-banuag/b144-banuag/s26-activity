//Use the require directive to load Node.js Modules
//"module" is a software component or part of a program that contains one or more routines
//"http module" - lets Node.js transfer data using the hypertext transfer protocol. it is a set of individual files that contain code to create a "component" that helps establish data transfer between applications

let http = require("http");
//Clients (browser) and servers (nodeJs/express js application) communicate by exchanging individual messages.

//Message sent by the client, usually a web browser are called requests
//http://home
//message sent by the server as an answer are called responses

//createServer() method - used to create an HTTP server that listens to requests on a specified port and gives responses back to the client
//it accepts a function and allows us to perform a certain task for our server
//request coming from client
//response coming from server
http.createServer(function(request, response) {

	//Use the writeHead method to:
	//Set a status code for he response - 200 means OK (successfull)
	//Set the content-type of the response as a plain text message

	response.writeHead(200, {'Content-Type': 'text/plain'});
	//We used the response.end() method the end the response process
	response.end("Goodbye")

}).listen(4000)
//A port is a virual point where network connections start and end
//Each port is associated with a specific process or service
//The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to our server

//When a server is running, console will print this message:
console.log('Server running at localhost:4000')

//Default - http://localhost:4000 (dito dadaan yung request and yung response)
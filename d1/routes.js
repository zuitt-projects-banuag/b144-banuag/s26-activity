const http = require('http');

//Create a varibale "port" to store the port number
const port = 4000

//Create a variable 'server' that store the output of the 'create server' method
const server = http.createServer((req, res) => {
	//url is a link of a browser
	//http://localhost:4000/greeting
	if(req.url == '/greeting'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Hello Again')
	} 

	//Access the /homepage routes returns a message of 'This is the homepage with a 200 status'
	if(req.url == '/home'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('This is the homepage with a 200 status')
	}

	//All other routes will return 'Page not found'
	else{
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end('Page not found')
	}

})

//Use the 'server' and 'port' variables created above
server.listen(port);

//When server is running, console will print the message:
console.log(`Server now accessible at localhost:${port}`)

//http://localhost:4000/home/about/contacts (routes)
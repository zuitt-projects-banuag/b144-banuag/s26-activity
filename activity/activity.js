- What directive is used by Node.js in loading the modules it needs? 
  ANSWER: import

- What Node.js module contains a method for server creation?
  ANSWER: http

- What is the method of the http object responsible for creating a server using Node.js?
  ANSWER: createServer()

- What method of the response object allows us to set status codes and content types?
  ANSWER: response.writeHead()

- Where will console.log() output its contents when run in Node.js?
  ANSWER: port

- What property of the request object contains the address's endpoint?
  ANSWER: response.end() 
